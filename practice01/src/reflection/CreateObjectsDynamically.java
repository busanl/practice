package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

//动态创建对象 通过反射
public class CreateObjectsDynamically {
    public static void main(String[] args) throws Exception{
        //获得class对象
        Class c1 = Class.forName("reflection.User");

        //构造一个对象
        User user1  = (User) c1.newInstance();
        System.out.println(user1);//调用本类的无参构造

        //通过构造器创建对象    获取指定参数的构造器
        Constructor declaredConstructor = c1.getDeclaredConstructor(int.class, String.class, int.class);
        User user2 = (User) declaredConstructor.newInstance(2, "小满", 18);
        System.out.println(user2);

        //通过反射调用普通方法
        User user3 = (User) c1.newInstance();
        Method setName = c1.getDeclaredMethod("setName", String.class);
        //invoke 激活的意思   （对象，方法的值）
        setName.invoke(user3,"小艾");
        System.out.println(user3.getName());

        //通过反射操作属性
        User user4 = (User) c1.newInstance();
        Field name = c1.getDeclaredField("name");
        //不能直接操作私有属性，我们需要关闭程序的安全检测，属性或者方法的
        name.setAccessible(true);
        name.set(user4,"小霞");
        System.out.println(user4.getName());
    }
}
