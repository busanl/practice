package CommonAlgorithms;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] arr1 = { 1, 2, 4, 3 };
        int[] arr = { 9, 5, 6, 8, 2, 7, 3, 4, 1 };
        int temp = 0; //用于交换
        boolean flag = false; //优化，判断循环是否发生交换

        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length-i-1; j++) {
                //如果前面的数比后面的数大，就直接交换
                if (arr[j] > arr[j+1]) {
                    flag = true;
                    temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }

            if (!flag) {
                break; //没有交换直接结束
            } else {
                flag = false; //重置flag,方便下一次循环使用
            }

            System.out.println("第" + (i+1) + "趟后的数组元素");
            System.out.println(Arrays.toString(arr));
        }

        //输出最后的结果
        System.out.println("最终结果:");
        for (int i : arr) {
            System.out.print(i + "  ");
        }
    }
}
