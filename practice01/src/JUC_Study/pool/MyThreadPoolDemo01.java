package JUC_Study.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池的优势：线程复用，控制最大并发数，管理线程。
 * Java中的线程池是通过 Executor 框架实现的，该框架中用到了 Executor ，Executors，
 * ExecutorService，ThreadPoolExecutor 这几个类。
 */
// Executors 工具类，里面有3大方法
// 使用了线程池之后，使用线程池来创建线程，用完记得关闭
public class MyThreadPoolDemo01 {
    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newSingleThreadExecutor(); //有且仅有单个线程
        //ExecutorService threadPool = Executors.newFixedThreadPool(5); //创建一个固定的线程池的大小
        //ExecutorService threadPool = Executors.newCachedThreadPool(); //可伸缩的

        try {
            for (int i = 0; i < 10; i++) {
                // 使用了线程池之后，使用线程池来创建线程
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+" OK");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //线程池用完，程序结束，关闭线程池
            threadPool.shutdown();
        }
    }
}
