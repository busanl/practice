package JUC_Study.pool;

import java.util.concurrent.*;

/**
 * 源码：
 * 7个参数：
 * public ThreadPoolExecutor(int corePoolSize, //核心线程池大小
 *                               int maximumPoolSize, //最大核心线程池大小
 *                               long keepAliveTime, //超时多久没有人调用就会释放
 *                               TimeUnit unit, //超时单位
 *                               BlockingQueue<Runnable> workQueue, //阻塞队列
 *                               ThreadFactory threadFactory, //线程工厂，创建线程的，一般不用动
 *                               RejectedExecutionHandler handler) //拒绝策略（四种）
 * 4种拒绝策略：
 * rejected = new ThreadPoolExecutor.AbortPolicy(); //默认，队列满了丢任务，抛出异常
 * rejected = new ThreadPoolExecutor.DiscardPolicy(); //队列满了丢任务，不抛出异常【如 果允许任务丢失这是最好的】
 * rejected = new ThreadPoolExecutor.DiscardOldestPolicy(); //将最早进入队列的任务 删，之后再尝试加入队列
 * rejected = new ThreadPoolExecutor.CallerRunsPolicy(); //如果添加到线程池失败，那么 主线程会自己去执行该任务，回退
 */
public class MyThreadPoolDemo02 {
    public static void main(String[] args) {
        // 工作中一般自定义线程池，用ThreadPoolExecutor
        ExecutorService threadPool = new ThreadPoolExecutor(
                2,
                Runtime.getRuntime().availableProcessors(), //详见下面的拓展
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

        // 最大承载：Deque + max
        //调整 i 值进行测试
        try {
            for (int i = 0; i < 9; i++) {
                // 使用了线程池之后，使用线程池来创建线程
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+" OK");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //线程池用完，程序结束，关闭线程池
            threadPool.shutdown();
        }
    }
}
/**
 * 拓展：
 * 池的最大的大小如何去设置？
 * 两种方法：
 * 1、CPU 密集型：几核，就是几，可以保持CPU的效率最高  // 获得CPU的内核数 System.out.println(Runtime.getRuntime().availableProcessors());
 * 2、IO 密集型：判断你程序中十分耗IO的线程数，然后再设置大于它的数
 */