package JUC_Study.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 前言：
 * Callable接口类似于Runnable，因为它们都是为其实例可能由另一个线程执行的类设计的
 * 然而，Runnable不返回结果，也不能抛出异常
 */
/**
 * 细节：
 * 1、有缓存
 * 2、结果可能需要等待。会阻塞
 */
public class CallableTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // new Thread(new Runnable()).start();
        // new Thread(new FutureTask<V>(Callable)).start();
        // 怎么启动Callable

        MyThread thread = new MyThread();
        FutureTask futureTask = new FutureTask(thread); // 适配类 ，具体可点进去看源码

        new Thread(futureTask,"A").start(); // 调用执行
        new Thread(futureTask,"B").start(); // 第二次调用执行，会有结果缓存，不用再次计算，效率高

        Integer o = (Integer) futureTask.get(); // get 方法获得返回结果! 一般放在最后一行！否则可能会阻塞
        // 或者使用异步通信来处理
        System.out.println(o);
    }
}

class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println("call 被调用");
        return 888;
    }
}
