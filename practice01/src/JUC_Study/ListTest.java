package JUC_Study;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

// java.util.ConcurrentModificationException 并发修改异常
public class ListTest {
    public static void main(String[] args) {
        // 并发下 ArrayList 不安全的
        /**
         * 解决方案：
         * 1、List<String> list = new Vector<>(); 【底层是synchronized，效率不高】
         * 2、List<String> list = new Collections.synchronizedList(new ArrayList<>());
         * 3、List<String> list = new CopyOnWriteArrayList<>(); 【推荐！底层是lock】
         */
        // CopyOnWrite 写入时复制 COW 计算机程序设计领域的一种优化策略
        /**
         * 原理：
         * CopyOnWriteArrayList容器允许并发读，读操作是无锁的，性能较高。
         * 至于写操作，比如向容器中添加一个元素，则首先将当前容器复制一份，
         * 然后在新副本上执行写操作，结束之后再将原容器的引用指向新容器。
         * 详见：https://www.cnblogs.com/chengxiao/p/6881974.html
         */

        List<String> list = new CopyOnWriteArrayList<>();

        for (int i = 1; i <= 10; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}
