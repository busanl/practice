package JUC_Study.count;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

// 信号量
public class SemaphoreDemo {
    public static void main(String[] args) {

        // 模拟资源类，有3个空车位
        Semaphore semaphore = new Semaphore(3);

        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire(); // acquire() 得到
                    System.out.println(Thread.currentThread().getName()+"抢到车位");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+"离开车位");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release(); // release() 释放
                }
            },String.valueOf(i)).start();
        }
    }
}

    /**
     * 原理：
     * 在信号量上定义两种操作：
     * acquire（获取）
     *      当一个线程调用 acquire 操作时，他要么通过成功获取信号量（信号量-1）
     *      要么一直等下去，直到有线程释放信号量，或超时
     * release （释放）
     *      实际上会将信号量的值 + 1，然后唤醒等待的线程。
     * 信号量主要用于两个目的：1、用于多个共享资源的互斥使用。2、用于并发限流，控制最大的线程数。
     */
