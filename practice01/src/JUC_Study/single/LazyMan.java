package JUC_Study.single;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

// 懒汉式单例
public class LazyMan {

    private static boolean mi_yao = false; // 红绿灯法

    private LazyMan(){
        synchronized (LazyMan.class) {
            if (mi_yao == false) {
                mi_yao = true;
            } else {
                throw new RuntimeException("不要试图使用反射破坏 异常！");
            }
        }
    }

    private volatile static LazyMan lazyMan; // 加volatile，防止指令重排

    // 双重检测锁模式的 懒汉式单例 DCL懒汉式
    public static LazyMan getInstance(){
        if (lazyMan == null){
            synchronized (LazyMan.class){
                if (lazyMan == null){
                    lazyMan = new LazyMan(); //不是原子性操作 可能会发生指令重排
                }
            }
        }
        return lazyMan;
    }

    // 模拟多线程开发
//    public static void main(String[] args) {
//        for (int i = 0; i < 10; i++) {
//            new Thread(()->{
//                LazyMan.getInstance();
//            }).start();
//        }
//    }

    // 利用 反射 破坏单例
    public static void main(String[] args) throws Exception {
//        LazyMan lazyMan1 = LazyMan.getInstance();

        Field mi_yao = LazyMan.class.getDeclaredField("mi_yao");
        mi_yao.setAccessible(true);

        Constructor<LazyMan> declaredConstructor = LazyMan.class.getDeclaredConstructor(null);
        declaredConstructor.setAccessible(true);

        LazyMan lazyMan1 = declaredConstructor.newInstance();

        mi_yao.set(lazyMan1,false);

        LazyMan lazyMan2 = declaredConstructor.newInstance();

        System.out.println(lazyMan1.hashCode());
        System.out.println(lazyMan2.hashCode());
    }
}
