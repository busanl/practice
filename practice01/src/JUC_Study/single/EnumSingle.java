package JUC_Study.single;

import java.lang.reflect.Constructor;

// 最后 发现 枚举可以解决
public enum EnumSingle {
    INSTANCE;
    public EnumSingle getInstance(){
        return INSTANCE;
    }
}

class Test{
    public static void main(String[] args) throws Exception {
        EnumSingle singleton1 = EnumSingle.INSTANCE;
        EnumSingle singleton2 = EnumSingle.INSTANCE;
        System.out.println("正常情况下，实例化两个实例是否相同："+ (singleton1 == singleton2));

        //Constructor<EnumSingleton> constructor = EnumSingleton.class.getDeclaredConstructor(); //自身的类没有无参构造方法
        // 尝试破坏  报异常：Cannot reflectively create enum objects
        Constructor<EnumSingle> constructor = EnumSingle.class.getDeclaredConstructor(String.class, int.class);
        constructor.setAccessible(true);
        EnumSingle enumSingle = constructor.newInstance();
        System.out.println(enumSingle);
    }
}
