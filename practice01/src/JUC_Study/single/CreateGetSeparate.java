package JUC_Study.single;

// 因为我们只需要在创建类的时候进行同步，所以只要将创建和getInstance()分开，单独为创建加synchronized关键字
// 解决了：懒汉式单例下（lazyMan = new LazyMan(); //不是原子性操作 可能会发生指令重排）的问题
// (如果不考虑用反射去破坏的话，这样实现是可以的)
public class CreateGetSeparate {

    private static CreateGetSeparate instance = null;

    private CreateGetSeparate() {}

    private static synchronized void syncInit() {
        if (instance == null) {
            instance = new CreateGetSeparate();
        }
    }

    public static CreateGetSeparate getInstance() {
        if (instance == null) {
            syncInit();
        }
        return instance;
    }

}
