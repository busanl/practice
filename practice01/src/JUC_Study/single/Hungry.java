package JUC_Study.single;

// 饿汉式单例

/*
优点：保证了线程的安全。
缺点：定义了四个byte数组，当代码一运行，这四个数组就被初始化，并且放入内存了，如
    果长时间没有用到getInstance方法，不需要Hungry类的对象，这是一种浪费。
 */
public class Hungry {

    // (模拟)  可能会浪费空间
    private byte[] data1 = new byte[1024*1024];
    private byte[] data2 = new byte[1024*1024];
    private byte[] data3 = new byte[1024*1024];
    private byte[] data4 = new byte[1024*1024];

    private Hungry(){

    }

    private final static Hungry HUNGRY = new Hungry();

    public static Hungry getInstance(){
        return HUNGRY;
    }
}
