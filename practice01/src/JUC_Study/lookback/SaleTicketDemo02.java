package JUC_Study.lookback;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 真正的多线程开发，公司中的开发
 * 线程就是一个单独的资源类，没有任何附属的操作！
 * 1、 属性、方法
 */

/**
 * Synchronized 和 Lock 区别：
 * 1、Synchronized 内置的Java关键字，Lock是一个Java类
 * 2、Synchronized 无法判断获取锁的状态，Lock可以判断是否获取到了锁
 * 3、Synchronized 会自动释放锁，Lock必须要手动释放锁！如果不释放锁，死锁
 * 4、Synchronized 线程1（获得锁，阻塞）、线程2（等待，傻傻的等）；Lock锁就不一定会傻傻地等下去，trylock
 * 5、Synchronized 可重入锁，不可以中断的，非公平；Lock。可重入锁，可以判断锁，非公平（可以自己设置成公平）
 *               //什么是 “可重入”，可重入就是说某个线程已经获得某个锁，可以再次获取锁而不会出现死锁。
 * 6、Synchronized 适合锁少量的代码同步问题，Lock适合锁大量的同步代码
 */

// Lock
public class SaleTicketDemo02 {
    public static void main(String[] args) {
        // 并发：多线程操作同一个资源类，把资源类丢入线程
        Ticket2 ticket = new Ticket2();

        // lambda表达式 (参数)->{ 代码 }
        new Thread(()->{
            for (int i = 1; i < 40; i++) {
                ticket.sale();
            }
        },"A").start();
        new Thread(()->{
            for (int i = 1; i < 40; i++) {
                ticket.sale();
            }
        },"B").start();
        new Thread(()->{
            for (int i = 1; i < 40; i++) {
                ticket.sale();
            }
        },"C").start();
    }
}

//资源类 OOP
/**
 * lock三部曲
 * 1、new ReentrantLock();
 * 2、lock.lock(); //加锁
 * 3、finally => lock.unlock(); //解锁
 */
class Ticket2{
    // 属性、方法
    private int number = 30;

    Lock lock = new ReentrantLock();

    public void sale(){

        lock.lock(); // 加锁

        try {
            // 业务代码
            if (number > 0){
                System.out.println(Thread.currentThread().getName()+"卖出了"+(number--)+"票，剩余："+number);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock(); // 解锁
        }


    }
}
