package JUC_Study.lookback;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//实现精准通知唤醒

/**
 * 题目：多线程之间按顺序调用，实现 A->B->C 依次循环
 * 重点：标志位
 */
public class ProducerAndConsumer03 {
    public static void main(String[] args) {
        Resources resources = new Resources();
        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                resources.printA();
            }
        }, "AA").start();
        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                resources.printB();
            }
        }, "BB").start();
        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                resources.printC();
            }
        }, "CC").start();
    }
}

// 资源类
class Resources {

    private int number = 1;// 1A 2B 3C

    private Lock lock = new ReentrantLock();
    private Condition condition1 = lock.newCondition();
    private Condition condition2 = lock.newCondition();
    private Condition condition3 = lock.newCondition();

    public void printA() {
        lock.lock();
        try {
            // 判断
            while (number != 1) {
                condition1.await();
            }
            // 干活
            System.out.println(Thread.currentThread().getName() + "=>AAAAAA");
            // 通知,指定的干活！
            number = 2;
            condition2.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void printB() {
        lock.lock();
        try {
            // 判断
            while (number != 2) {
                condition2.await();
            }
            // 干活
            System.out.println(Thread.currentThread().getName() + "=>BBBBBB");
            // 通知,指定的干活！
            number = 3;
            condition3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void printC() {
        lock.lock();
        try {
            // 判断
            while (number != 3) {
                condition3.await();
            }
            // 干活
            System.out.println(Thread.currentThread().getName() + "=>CCCCCC");
            // 通知,指定的干活！
            number = 1;
            condition1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}