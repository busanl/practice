package Demo.demo03;

/**
 * 执行顺序：
 * 1、静态初始化块
 * 2、普通初始化块
 * 3、构造器
 */
public class HelloB extends HelloA {
    public HelloB(){
        System.out.println("constructor B");
    }
    {
        System.out.println("I am B class");
    }
    static {
        System.out.println("static B");
    }

    public static void main(String[] args) {
        new HelloB();
    }
}

class HelloA{
    public HelloA(){
        System.out.println("constructor A");
    }
    {
        System.out.println("I am A class");
    }
    static {
        System.out.println("static A");
    }
}