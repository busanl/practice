package Demo.demo03;

/**
 * 笔试笔记：
 * spring自动装配有哪几种方式？
 * @Resource和@Autowrid区别？
 * #{}和 ${}的区别？
 * final、finally与finalize的区别？
 * ==和equals的区别？
 * 重载和重写是什么？两者区别？
 * 1、构造方法不能重写，但可以重载
 * 2、不能通过返回类型区分重载
 *
 * 数据库操作语句
 */

public class Test5 {
    public static void main(String[] args) {

        //为什么报错？

        //float ss = 3.1; //在java里面，没小数点的默认是int，有小数点的默认是 double;

        //short ss1 = 1;ss1 = ss1 + 1;
/*
short s1 = 1; s1 = s1 + 1;和 short s1 = 1; s1 += 1;的问题：
对于short s1 = 1; s1 = s1 + 1; 由于short+int编译器为了避免内存溢出，就给它向上转型(int)，所以结果是int型，再赋值给short类型s1时，编译器将报告需要强制转换类型的错误。
对于short s1 = 1; s1 += 1; 由于 += 是java语言规定的运算符，java编译器会对它进行特殊处理（强转），因此可以正确编译。
*/

        //多提一句：byte，short，char参与运算时一律自动转换为int

        System.out.println("=======================");

        System.out.println( 240 == 240.0); //true

        System.out.println("=======================");

        String s1 = "ab";
        String s2 = "ab";
        String s3 = new String("aaa");
        String s4 = new String("aaa");
        System.out.println(s1 == s2); //true
        System.out.println(s3 == s4); //false

        System.out.println("=======================");

        int num1 = 100;
        int num2 = 20;
        swap(num1, num2);
        System.out.println(num1); //100
        System.out.println(num2); //20
    }

    private static void swap(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
        System.out.println(a); //20
        System.out.println(b); //100
    }
}
