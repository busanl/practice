package Demo.demo03;

/**
 * 网上的说法：
 * 除了基本类型之外的，所有类型，包括数组的赋值，都是引用传递
 * （1）基本数据类型传值，对形参的修改不会影响实参；
 * （2）引用类型传引用，形参和实参指向同一个内存地址（同一个对象），所以对参数的修改会影响到实际的对象；
 * 其实就是操作的是一块内存还是新开辟了一块内存的区别
 */
public class Test3 {
    public static void main(String[] args) {
        Test3 t = new Test3();
        int a = 99;
        t.testA(a);//这里传递的参数a就是按值传递
        System.out.println(a);

        MyObj obj = new MyObj();
        t.testB(obj);//这里传递的参数obj就是引用传递
        System.out.println(obj.b);
    }

    public void testA(int a){
        a++;
        System.out.println(a);
    }

    public void testB(MyObj obj){
        obj.b = 100;
        System.out.println(obj.b);
    }
}

class MyObj{
    public int b=99;
}
