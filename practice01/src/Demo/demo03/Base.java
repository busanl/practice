package Demo.demo03;

public class Base {

    private String baseName = "base";
    public Base(){
        callName();
    }

    public void callName(){
        System.out.println(baseName);
    }

    static class Sub extends Base{ //继承
        private String baseName = "sub";
        public void callName(){
            System.out.println(baseName);
        }
    }

    public static void main(String[] args) {
        Base b = new Sub(); // 输出结果：null
    }
}
/**
 * Base b = new Sub();在创建派生类（子类）的过程中首先创建基类（父类）对象，然后才能创建派生类
 * 创建基类即默认调用Base()构造方法，在方法中调用callName()方法，而父类和子类都有callName()这个
 * 方法，所以这是多态【
 * 1、必须要有继承
 * 2、必须要有重写
 * 3、父类的引用指向子类的对象（实体）】
 * 所以public Base(){callName();}中的callName()方法指向Sub（子类）的callName()方法，
 * 然而此时子类还未构造，baseName这个参数还未被赋值，所以变量baseName的值为null
 *
 * 有问题说Sub类是静态的，不是应该先被加载吗？怎么说这个类还未构造？
 * 网友解答说：巴拉巴拉...总的来说，就是static的作用相当于建立了2个类，而不是这个有static修饰的
 * 类先加载，这两个类的关系就是单纯的继承关系
 */