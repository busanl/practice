package Demo.demo03;

import java.util.concurrent.CountDownLatch;

/**
 * main线程执行完不会立马关闭，所以测试的时候需要加上==》System.exit(0);手动关闭
 * 实际生成中主线程方法执行完毕以后子线程直接关闭
 * 当然，有许多方法可以让主线程等待子线程结束后再结束
 */
public class Test6 {
    public static void main(String[] args) {

        final CountDownLatch latch = new CountDownLatch(1);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100000000; i++) {
                    if (i==99999999){
                        System.out.println("我现在是99999999，这是子线程干的活");
                        latch.countDown();
                    }
                }
            }
        });
        thread.start();
        System.out.println(Thread.currentThread().getName()+"结束了 ，这是main干的活");

        try {
            latch.await();
            System.out.println("所有子线程执行完毕了。。。");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }
}
