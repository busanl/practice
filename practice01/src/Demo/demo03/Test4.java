package Demo.demo03;

//哈希值和地址值是不一样的，哈希值是通过哈希算法散列得来的，
//而地址值是通过和物理层面有关，是系统分匹配的，是不存在相同的，
//而哈希值是可以通过强制手段设置为相同的，
//也就是说哈希值是一种逻辑上的确保唯一性，而地址值就是物理上确保唯一性
public class Test4 {
    public static void main(String[] args) {
        String s1 = "通话";
        String s2 = "重地"; // 经典，这两个hashcode才相等
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode()); // 两个对象有相同的hashcode值，它们也不一定是相等的

        String s3 = "童话";
        System.out.println(s3.hashCode());
    }
}
