package Demo.demo03;

public class Test1 {
    public static void main(String[] args) {
        int i = -5;
        //i = ++(i++); //i++是字面量 报错！
        //i = ++(10); //报错！
        System.out.println(i);

        //什么是字面量？
        //int b = 10; //b为常量，10为字面量
    }
}
