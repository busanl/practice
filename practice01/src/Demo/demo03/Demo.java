package Demo.demo03;

public class Demo {
    public static void main(String[] args) {
        int count = 0;
        int num = 0;
        for (int i = 0; i <= 100; i++) {
            num = num + i;
            count = count++; //输出结果：num * count = 0
            //count++; //输出结果：num * count = 510050
        }
        System.out.println("num * count = " + (num * count));
        System.out.println(count);
        System.out.println(num);
    }
}
/**
 * int i = 1; int j = 0;
 * j = i++;
 * System.out.println(j + " " + i)//输出结果：1 2
 * 上面这个代码，在深入了解这个题目之前，我理解的这背后的原理是这样的；
 * - 由于i++，是后加1，所以先执行的是j=i; 所以j=1;
 * - 然后自增1，所以i=2;
 * 但是如果是这样count=0时执行count = count++; 应该count =1 才对。
 *
 * 其实问题的关键是java在执行自增的时候是先将i保存在一个临时变量中，然后再自增：
 * 所以j = i++ 实际的执行顺序是：
 * - 先将i保存到临时变量中：temp = i;
 * - 然后自增 i = i+1;
 * - 然后j = temp给j赋值；
 * 看明白了么，之前我的理解一直的j先赋值，然后i才加1的。
 * 实际是i先加一，然后j才赋值，只是这个时候j赋值的是temp，而不是i；
 * 所以count=0时count=count++;之后count=0;
 */