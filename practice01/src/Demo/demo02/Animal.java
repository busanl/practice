package Demo.demo02;

//乱测
public class Animal {

    private String name;

    public static final int CURRENT_NUM = 10;

    private static int numInstances = 2;

    protected static int getCount() {
        return numInstances;
    }

    public static int backnum1(int i, int j){
        return i + j;
    }

    public int backnum2(int i, int j){
        return i + j;
    }

    public int usebacknum(){
        return backnum1(1,2);
    }

    public void aaa(){
        Integer i = 100;
        Integer j = Integer.valueOf(100);
        Integer k = new Integer(100);
        System.out.println(i == j); //true
        System.out.println(i == k); //false
        System.out.println(j == k); //false
    }
}
