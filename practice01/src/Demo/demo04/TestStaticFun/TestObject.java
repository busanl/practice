package Demo.demo04.TestStaticFun;

public class TestObject {

    private static TestObject to = new TestObject();

    private TestObject() {}

    public static TestObject getIns() {
        return to;
    }

    public static void getTest() {
        TestStatic ts = new TestStatic();
        System.out.println(ts);
    }
}

