package Demo.demo04.TestStaticFun;

/**
 * 由此可知
 * 静态方法中创建的对象是不共享的（不是同一个对象）
 */
public class TestStatic {

    private int i = 0;

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            TestObject object = TestObject.getIns();
//            System.out.println(object);

            TestObject.getTest();
        }
    }
}