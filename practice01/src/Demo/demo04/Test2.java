package Demo.demo04;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Test2 {

    public static volatile int ii = 0;

    public static void main(String[] args) {
        long l = 15552451L;
        double d = Double.longBitsToDouble(l/1000);
        double dd = (double)l/1000;
        System.out.println(dd);

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        System.out.println(dateFormat.format(date));

        Set<Integer> set = new HashSet<Integer>();
        set.add(10);
        set.add(20);
        set.add(10);
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        String str1 = new String("abc");
        String str2 = "abc";
        String str3 = "abc";
        String str4 = new String("abc").intern();
        System.out.println(str1==str2);
        System.out.println(str2==str3);
        System.out.println(str2==str4);

        new Thread(() -> {
            while (ii == 0) {

            }
        });
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ii = 1;
        System.out.println("end");
    }
}
