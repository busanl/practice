package Demo.demo04;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Set;

/**
 * 死锁测试
 */
public class Test3 {

    private static final String A = "A";
    private static final String B = "B";

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            synchronized (A) {
                System.out.println("线程一获取到资源A");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程一尝试获取资源B......");
                synchronized (B) {
                    try {
                        System.out.println("线程一获取到资源B");
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setName("死锁一号");
        thread.start();

        Thread t2 = new Thread(() -> {
            synchronized (B) {
                System.out.println("线程二获取到资源B");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程二尝试获取资源A......");
                synchronized (A) {
                    try {
                        System.out.println("线程二获取到资源A");
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t2.setName("死锁二号");
        t2.start();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("=======================");
        ThreadMXBean mbean = ManagementFactory.getThreadMXBean();
        // 只能检测 synchronized 同步代码块的死锁
        // long[] deadlockedThreadIds = mbean.findMonitorDeadlockedThreads();
        // 可以检测 juc 下的 Lock造成的死锁和 synchronized 代码块的死锁
        long[] deadlockedThreadIds = mbean.findDeadlockedThreads();
        if (deadlockedThreadIds != null) {
            // 打印线程信息
            ThreadInfo[] threadInfos = mbean.getThreadInfo(deadlockedThreadIds);
            for (ThreadInfo ti : threadInfos) {
                System.out.println(ti);
            }
            // 中断死锁线程
            Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();
            for (int i = 0; i < deadlockedThreadIds.length; i++) {
                for(Thread t : setOfThread){
                    if(t.getId( ) == deadlockedThreadIds[i]) {
                        System.out.println("in");
                        t.interrupt();
                    }
                }
            }
        }

    }
}
