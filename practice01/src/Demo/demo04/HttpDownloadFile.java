package Demo.demo04;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;

public class HttpDownloadFile {

    String utl = "http://{}:9072/file?path={}";

    public static void main(String[] args) {
        fileUrl("https://dn-odum9helk.qbox.me/Fvzr5I55m4wJYsXkTrUlqXIyhdl0","一岐日和","C:\\Users\\LJY\\Desktop");
    }

    /**
     * File url
     *
     * @param urlAddress url address
     * @param localFileName local file name
     * @param destinationDir destination dir
     */
    public static void fileUrl(String urlAddress, String localFileName, String destinationDir) {
        OutputStream outputStream = null;
        URLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            URL url = new URL(urlAddress);
            outputStream = new BufferedOutputStream(Files.newOutputStream(Paths.get(destinationDir + "\\" + localFileName + ".jpg")));
            urlConnection = url.openConnection();
            inputStream = urlConnection.getInputStream();

            byte[] buf = new byte[1024];
            int byteRead, byteWritten = 0;
            while ((byteRead = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, byteRead);
                outputStream.flush();
                byteWritten += byteRead;
            }
            System.out.println("下载完成");
        } catch (Exception e) {
//                log.error(e.getMessage());
        } finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
//                    log.error(e.getMessage());
            }
        }
    }

    /**
     * File download
     *
     * @param urlAddress url address
     * @param destinationDir destination dir
     */
    public static void fileDownload(String urlAddress, String destinationDir) {
        int slashIndex = destinationDir.lastIndexOf('/');
        int periodIndex = destinationDir.lastIndexOf('.');

        String fileName = destinationDir.substring(slashIndex + 1, periodIndex);

        if (periodIndex >= 1 && slashIndex >= 0 && slashIndex < urlAddress.length() - 1) {
            fileUrl(urlAddress, fileName, destinationDir);
        } else {
            System.err.println("path or file name.");
        }
    }

}
