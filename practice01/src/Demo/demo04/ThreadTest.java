package Demo.demo04;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {

    public void vvv() {
        System.out.println("============vvv 开始");
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    System.out.println("[vvv]我是run方法"+i);
                }
            }
        }).start();
        System.out.println("============vvv 结束");
    }

    public void xixi() {
        System.out.println("============xixi 开始");
        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        try {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 100; i++) {
                        System.out.println("[xixi]我是run方法"+i);
                    }
                }
            });
        } catch (Throwable ex) {
            ex.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
        System.out.println("============xixi 结束");
    }

}
