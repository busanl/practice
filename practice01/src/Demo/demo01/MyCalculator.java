package Demo.demo01;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.IOException;

public class MyCalculator extends JFrame implements ActionListener{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textField;
    private JPanel panel_1;
    private JButton btnBackpace,btnC;
    private JPanel panel_2;
    Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MyCalculator frame = new MyCalculator();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public MyCalculator() {
        super("计算器");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.NORTH);
        panel.setLayout(new BorderLayout(0, 0));



        textField = new JTextField("0");
        textField.addActionListener(this);
        panel.add(textField, BorderLayout.CENTER);
        textField.setColumns(10);

        panel_1 = new JPanel();
        panel.add(panel_1, BorderLayout.SOUTH);
        panel_1.setLayout(new BorderLayout(0, 0));

        btnBackpace = new JButton("Backpace");
        btnBackpace.addActionListener(this);
        panel_1.add(btnBackpace);

        btnC = new JButton("           C           ");
        btnC.addActionListener(this);
        panel_1.add(btnC, BorderLayout.EAST);

        JToolBar toolBar = new JToolBar();
        panel.add(toolBar, BorderLayout.NORTH);

        JMenu mnNewMenu = new JMenu("编辑(E)");
        toolBar.add(mnNewMenu);
        JMenuItem fuzhi = new JMenuItem("复制");
        JMenuItem zhantie = new JMenuItem("粘贴");
        mnNewMenu.add(fuzhi);
        mnNewMenu.add(zhantie);
        fuzhi.addActionListener(this);
        zhantie.addActionListener(this);
        JMenuBar bar = new JMenuBar();
        bar.add(mnNewMenu);
        setJMenuBar(bar);

        panel_2 = new JPanel();
        contentPane.add(panel_2, BorderLayout.CENTER);
        panel_2.setLayout(new GridLayout(0,4));

        String[] str = { "7", "8", "9", "/", "4", "5", "6", "*", "1", "2", "3", "-", ".", "0", "=", "+" };
        JButton[] bottons = new JButton[str.length];
        int i;
        for(i =0;i < str.length;i ++){
            bottons[i] = new JButton(str[i]);
            bottons[i].addActionListener(this);
            panel_2.add(bottons[i]);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        Object target = e.getSource();
        if(target == btnC){
            resetmethod();		//输入的是重置按钮
        }
        else if(target == btnBackpace){
            backmethod();		//输入的是退格按钮
        }
        else if ("0123456789.".indexOf(command) >= 0) {
            numbermethod(command);  //输入的是数字或小数点
        }
        else{
            operatormethod(command);	//输入的是运算符
        }
        //编辑选项中的复制与粘贴项
        if(command=="复制"){
            Transferable futext = new StringSelection(textField.getText());
            clip.setContents(futext, null);
        }
        if(command=="粘贴"){
            Transferable zhantext = clip.getContents(null);
            if (zhantext != null) {

                if (zhantext.isDataFlavorSupported(DataFlavor.stringFlavor))
                    try {
                        textField.setText((String)zhantext.getTransferData(DataFlavor.stringFlavor));
                    } catch (UnsupportedFlavorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
            }
        }
    }

    boolean thefirst = true;

    public void resetmethod(){//重置方法
        textField.setText("0");
        thefirst = true;
        operator = "=";//注意在点击CE之后一定要重置isFirstDight和operator,System.out.println("CE之后的isDight  "+isFirstDigit);
    }

    public void backmethod(){//退格方法
        String str = textField.getText();
        if (str.length() > 0) {
            textField.setText(str.substring(0, str.length() - 1));
        }
    }

    public void numbermethod(String command){
        if(thefirst){	//判断是不是第一次按下
            if(command.equals(".")){
                textField.setText("0.");
            }
            else{
                textField.setText(command);
            }
        }else if(command.equals(".")&&textField.getText().indexOf(".")<0){
            textField.setText(textField.getText() + ".");//判断该符号是不是点，然后确定小数点在之前有没有出现过
        }else if(!command.equals(".")){		//判断输入非首位的数字
            textField.setText(textField.getText() + command);
        }
        thefirst = false;
    }

    double number = 0.0;
    String operator = "=";
    public void operatormethod(String command){
        if(operator.equals("+")){
            //把数字类型的字符串，转换成Double类型
            number += Double.valueOf(textField.getText());
        }
        else if(operator.equals("-")){
            number -= Double.valueOf(textField.getText());
        }
        else if(operator.equals("*")){
            number *= Double.valueOf(textField.getText());
        }
        else if(operator.equals("/")){
            number /= Double.valueOf(textField.getText());
        }
        else if(operator.equals("=")){
            number = Double.valueOf(textField.getText());
        }
        textField.setText(String.valueOf(number));
        operator = command;
        thefirst = true;
    }
}

