package Demo.demo01;

import java.util.concurrent.atomic.AtomicInteger;

public class Helloworld {

    int ii =2;

    public static void main(String[] args) {

        System.out.println("Hello World!");
        int i = 1;
        System.out.println(i);

        AtomicInteger atomicInteger = new AtomicInteger(5);
        atomicInteger.getAndIncrement();


        Helloworld hello = new Helloworld();
        Inner aos = hello.new Inner();
        aos.outout();
    }

    class Inner {
        public void outout(){
            System.out.println(ii);
        }
    }
}
