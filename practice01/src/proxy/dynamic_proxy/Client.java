package proxy.dynamic_proxy;

public class Client {
    public static void main(String[] args) {
        //真实角色
        UserServiceImpl user = new UserServiceImpl();
        //代理角色，目前不存在
        ProxyInvocationHandler pih = new ProxyInvocationHandler();
        //设置要代理的对象
        pih.setTarget(user);
        //生成动态代理类
        UserService proxy = (UserService) pih.getProxy();

        proxy.add();
    }
}
