package proxy.static_proxy;

public class Client {
    public static void main(String[] args) {
        UserServiceImpl user = new UserServiceImpl();
        UserServiceImplProxy proxy = new UserServiceImplProxy();
        proxy.setUserService(user);
        proxy.add();
    }
}
