package NiuKe;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

    public class TreeSetTest {
        public static void main(String[] args) {
            TreeSet ts = new TreeSet(new Teacher.TeacherCompare());
            ts.add(new Teacher("zhangsan", 2));
            ts.add(new Teacher("lisi", 1));
            ts.add(new Teacher("wangmazi", 3));
            ts.add(new Teacher("mazi", 3));
            Iterator it = ts.iterator();
            while (it.hasNext()) {
                System.out.println(it.next());
            }
        }
    }

    class Teacher {
        int num;
        String name;

        Teacher(String name, int num) {
            this.num = num;
            this.name = name;
        }

        public String toString() {
            return "学号：" + num + "    姓名：" + name;
        }

        static class TeacherCompare implements Comparator {// 老师自带的一个比较器

            public int compare(Object o1, Object o2) {
                Teacher s1 = (Teacher) o1;// 转型
                Teacher s2 = (Teacher) o2;// 转型
                int result = s1.num > s2.num ? 1 : (s1.num == s2.num ? 0 : -1);
                if (result == 0) {
                    result = s1.name.compareTo(s2.name);
                }
                return result;
            }

        }
    }

