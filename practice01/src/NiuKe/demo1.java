package NiuKe;

import java.util.Scanner;

public class demo1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine().toLowerCase();
        char c = sc.nextLine().toLowerCase().charAt(0);
        System.out.println(num(str, c));
    }

    private static int num(String str, char c) {
        int i = 0;
        char[] chars = str.toCharArray();
        for (char ch : chars) {
            if(ch == c){
                i++;
            }
        }
        return i;
    }
}
