package NiuKe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class demo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("123");
        list.add("321");
        System.out.println(list);
        System.out.println("===========");
        Map<String,String> map = new HashMap<>();
        map.put("nihao","123");
        map.put("zhenbang","321");
        System.out.println(map);
        System.out.println("===========");
        int[] src = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(src[1]);
    }
}
