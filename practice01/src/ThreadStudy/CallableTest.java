package ThreadStudy;

import java.util.concurrent.*;

/**
 * 线程创建方式三：实现callable接口
 *
 * 好处：1、可以定义返回值。2、可以抛出异常。
 */
public class CallableTest implements Callable {

    /**
     * 图片路径
     */
    private String url;
    /**
     * 图片名称
     */
    private String name;

    public CallableTest(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public Boolean call() {
        webDownLoad webDownLoad = new webDownLoad();
        webDownLoad.downloader(url,name);
        System.out.println("图片下载成功"+name);
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CallableTest c1 = new CallableTest("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3175508956,2902264390&fm=26&gp=0.jpg","1.jpg");
        CallableTest c2 = new CallableTest("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1757873658,3096875923&fm=26&gp=0.jpg","2.jpg");
        CallableTest c3 = new CallableTest("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2655406508,1191616699&fm=26&gp=0.jpg","3.jpg");

        //创建执行服务：
        ExecutorService ser = Executors.newFixedThreadPool(3);

        //提交执行
        Future<Boolean> r1 = ser.submit(c1);
        Future<Boolean> r2 = ser.submit(c2);
        Future<Boolean> r3 = ser.submit(c3);

        //获取返回结果
        boolean rs1 = r1.get();
        boolean rs2 = r2.get();
        boolean rs3 = r3.get();

        System.out.println(rs1);
        System.out.println(rs2);
        System.out.println(rs3);

        //关闭服务
        ser.shutdownNow();
    }

}


