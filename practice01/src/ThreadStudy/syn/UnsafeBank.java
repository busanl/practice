package ThreadStudy.syn;

//不安全地取钱
//两个人去银行取钱，账户
public class UnsafeBank {
    public static void main(String[] args) {
        //账户
        Account account = new Account(100, "结婚基金");

        Drawing you = new Drawing("你", account, 50);
        Drawing girlFriend = new Drawing("girlFriend", account, 100);

        you.start();
        girlFriend.start();
    }
}

//账户
class Account{
    int money;//余额
    String name;//卡名

    public Account(int money, String name) {
        this.money = money;
        this.name = name;
    }
}

//银行：模拟取款
class Drawing extends Thread{
    Account account;//账户
    //取了多少钱
    int drawingMoney;
    //现在手里还有多少钱
    int nowMoney;

    public Drawing(String name, Account account, int drawingMoney){
        super(name);
        this.account = account;
        this.drawingMoney = drawingMoney;;
    }

    //取钱
    @Override
    public void run() {
        //判断有咩有钱
        if(account.money - drawingMoney < 0){
            System.out.println(Thread.currentThread().getName() + "钱不够，取不了");
            return;
        }

        //模拟延时(sleep可以放大问题的发生性)
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //卡内余额 = 余额 - 你取的钱
        account.money = account.money - drawingMoney;
        //你手里的钱
        nowMoney = nowMoney + drawingMoney;

        System.out.println(account.name + "余额为：" + account.money);
        //Thread.currentThread().getName() = this.getName()
        System.out.println(this.getName() + "手里的钱：" + nowMoney);
    }
}