package ThreadStudy.syn.safe;

//不安全地买票
public class SafeBuyTicket {

    public static void main(String[] args) {

        BuyTicket station = new BuyTicket();

        new Thread(station,"上班族").start();
        new Thread(station,"暴发户").start();
        new Thread(station,"黄牛党").start();
    }
}

class BuyTicket implements Runnable {

    //票
    private int ticketNums = 100;
    boolean flag = true; //外部停止方式

    @Override
    public void run() {
        while (flag){
            buy();
        }
    }

    // synchronized 同步方法，锁的是this
    private synchronized void buy(){
        //判断是否有票
        if(ticketNums <= 0){
            flag = false;
            return;
        }
        //模拟延时
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //买票
        System.out.println(Thread.currentThread().getName() + "拿到" + ticketNums--);
    }
}
