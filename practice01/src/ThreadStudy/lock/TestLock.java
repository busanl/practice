package ThreadStudy.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * java.util.concurrent.locks.Lock 接口是控制多个线程对共享资源进行访问的工具。
 * ReentrantLock 类实现了Lock，它拥有与 synchronized 相同的并发性和内存语义
 * 在实现线程安全的控制中，比较常用的是ReentrantLock，可以显式加锁、释放锁
 *
 * synchronized 与 Lock 的对比
 * - Lock是显式锁（手动开启和关闭锁，别忘记关闭锁）synchronized是隐式锁，出作用域自动释放
 * - 使用Lock锁，JVM将花费较少的时间来调度线程，性能更好。并且具有更好的拓展性（提供更多的子类）
 * - 优先使用顺序：Lock > 同步代码块 > 同步方法
 */
// 测试 lock 锁
public class TestLock {
    public static void main(String[] args) {
        TestLock2 testLock2 = new TestLock2();

        new Thread(testLock2).start();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
    }
}

class TestLock2 implements Runnable {

    int ticketNums = 10;

    //定义lock锁
    private final ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true){
            try {
                lock.lock(); // 加锁
                if (ticketNums > 0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(ticketNums--);
                }else {
                    break;
                }
            }finally {
                lock.unlock(); // 解锁
            }
        }
    }
}