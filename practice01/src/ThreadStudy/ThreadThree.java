package ThreadStudy;

/**
 * 多个线程同时操作同一个对象
 * 买火车票的例子
 */
public class ThreadThree implements Runnable{

    private int ticketNums = 10;

    @Override
    public void run() {
        while(true){
            if (ticketNums<=0){
                break;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"——>拿到了第"+ticketNums--+"张票");

        }
    }

    public static void main(String[] args) {
        ThreadThree threadThree = new ThreadThree();

        new Thread(threadThree,"小明").start();
        new Thread(threadThree,"老师").start();
        new Thread(threadThree,"老黄牛").start();
    }

}
