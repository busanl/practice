package ThreadStudy;

//import org.apache.commons.io.FileUtils;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URL;

public class DownloadPic extends Thread {
    /**
     * 图片路径
     */
    private String url;
    /**
     * 图片名称
     */
    private String name;

    public DownloadPic(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        webDownLoad webDownLoad = new webDownLoad();
        webDownLoad.downloader(url,name);
        System.out.println("图片下载成功"+name);
    }

    public static void main(String[] args) {
        DownloadPic d1 = new DownloadPic("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3175508956,2902264390&fm=26&gp=0.jpg","1.jpg");
        DownloadPic d2 = new DownloadPic("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1757873658,3096875923&fm=26&gp=0.jpg","2.jpg");
        DownloadPic d3 = new DownloadPic("https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2655406508,1191616699&fm=26&gp=0.jpg","3.jpg");
        //先是图片1
        d1.start();
        //再是图片2
        d2.start();
        //最后是图片3
        d3.start();
        //事实
//        图片下载成功2.jpg
//        图片下载成功3.jpg
//        图片下载成功1.jpg
        //或其他顺序
    }
}

class webDownLoad{
    //下载方法
    public void downloader(String url,String name){
//        try {
//            //FileUtils.copyURLToFile(new URL(url),new File(name));
//        } catch (IOException e) {
//            e.printStackTrace();
//            System.out.println("Io异常，download方法出现问题");
//        }

    }
}
