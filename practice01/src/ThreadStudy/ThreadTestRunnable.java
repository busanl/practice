package ThreadStudy;

//创建线程方式2 ：实现runnable接口，重写run方法，执行线程需要丢入runnable接口实现类，调用start方法
public class ThreadTestRunnable implements Runnable{
    @Override
    public void run() {
        //run方法线程体
        for (int i = 0; i < 500; i++) {
            System.out.println("我是run方法"+i);
        }
    }

    //main线程，主线程
    public static void main(String[] args) {

        //创建runnable接口的实现类对象
        ThreadTestRunnable t1 = new ThreadTestRunnable();

        //创建线程对象，通过线程对象来开启我们的线程，代理
        new Thread(t1).start();

        for (int i = 0; i < 500; i++) {
            System.out.println("我是主线程"+i);
        }
    }
}
