package ThreadStudy;

//创建线程方法一：继承Thread类，重写run()方法，调用start开启线程
public class ThreadTest extends Thread {
    @Override
    public void run() {
        //run方法线程体
        for (int i = 0; i < 500; i++) {
            System.out.println("我是run方法"+i);
        }
    }

    //main线程，主线程
    public static void main(String[] args) {

        ThreadTest threadTest = new ThreadTest();

        //run方法 单细胞死脑筋一条路走到黑
        //threadTest.run();

        //start方法 多线程并发
        threadTest.start();

        for (int i = 0; i < 500; i++) {
            System.out.println("我是主线程"+i);
        }
    }
}
