package Leetcode;

import java.util.ArrayDeque;

public class Solution_20 {
    public static boolean isValid(String s) {
        if (s.length() % 2 == 1) return false;
        ArrayDeque<Character> stack = new ArrayDeque<>();
        for (char c : s.toCharArray()) {
            if (c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else {
                if (stack.isEmpty()) return false;
                char top = stack.pop();
                if (c == ')' && top != '(') return false;
                if (c == '}' && top != '{') return false;
                if (c == ']' && top != '[') return false;
            }
        }
        return stack.isEmpty();
    }

    public static boolean isValid_2(String s) {
        int index = s.length()/2;
        for(int i=0;i<index;i++) {
            s = s.replace("()", "").replace("{}", "").replace("[]", "");
        }
        return s.length()==0;
    }

    public static void main(String[] args) {
        String s = "{}[()]";
        System.out.println(isValid(s));
        System.out.println(isValid_2(s));

        System.out.println("=======================");

        String ss = "[{{}}[()]";
        String s1 = ss.replace("()", "").replace("{}", "").replace("[]", "");
        System.out.println(s1);
    }
}
