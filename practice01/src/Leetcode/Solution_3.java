package Leetcode;

import java.util.HashSet;
import java.util.Set;

public class Solution_3 {
    public static int lengthOfLongestSubstring_1(String s) {
        int n = s.length();
        if(n == 0) return 0;
        int maxLength = 1;

        int left = 0, right = 0;
        Set<Character> window = new HashSet<>();
        while(right < n){
            char rightchar = s.charAt(right);
            while(window.contains(rightchar)) {
                window.remove(s.charAt(left));
                left++;
            }
            maxLength = Math.max(maxLength, right-left+1);
            window.add(rightchar);
            right++;
        }
        return maxLength;
    }

    public static int lengthOfLongestSubstring_2(String s) {
        if(s.equals("")){
            return 0;
        }
        String[] strArray = s.split("");
        int flag = 1;
        int max = 0;
        int mark;
        for(int i = 0; i < strArray.length; i++){
            for(int j = i+1; j < strArray.length; j++){
                mark = i;
                while(i < j){
                    if(strArray[i].equals(strArray[j])){
                        break;
                    }
                    i++;
                }
                if(i == j){
                    flag++;
                    i = mark;
                } else {
                    i = mark;
                    break;
                }
            }
            if(flag > max) {
                max = flag;
            }
            flag = 1;
        }
        return max;
    }

    public static void main(String[] args) {
        String s = "pwwkew";
        System.out.println(lengthOfLongestSubstring_1(s));
    }
}
