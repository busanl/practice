package Leetcode;

import java.util.*;

public class Solution_1 {
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> hm = new HashMap<>();
        for(int i=0; i < nums.length; i++) {
            if(hm.containsKey(target-nums[i])) {
                return new int[]{ hm.get(target-nums[i]), i };
            }else{
                hm.put(nums[i], i);
            }
        }
        return new int[0];
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2,7,11,15};
        int target = 26;
        int[] ts = twoSum(nums, target);
        for(int t : ts) {
            System.out.print(t + " ");
        }
    }
}
