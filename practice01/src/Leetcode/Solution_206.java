package Leetcode;

public class Solution_206 {
    public static ListNode reverseList(ListNode head) {
        if(head==null || head.next==null){
            return head;
        }
        ListNode p = reverseList(head.next);
        head.next.next = head;
        head.next = null;
        return p;
    }

    public static void main(String[] args) {
        ListNode Ln = new ListNode(1);
        Ln.next = new ListNode(2);
        Ln.next.next = new ListNode(3);
        Ln.next.next.next = new ListNode(4);
        Ln.next.next.next.next = new ListNode(5);

        ListNode lend = reverseList(Ln);
        while (lend != null) {
            System.out.print(lend.val + ",");
            lend = lend.next;
        }
    }
}
