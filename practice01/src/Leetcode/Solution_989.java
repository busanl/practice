package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution_989 {
    public static List<Integer> addToArrayForm(int[] num, int k) {
        List<Integer> res = new ArrayList<>();
        int carry = 0;
        int l1 = num.length - 1;

        while(l1 >= 0 || k != 0){
            int num1 = l1 < 0 ? 0 : num[l1];
            int num2 = k == 0 ? 0 : k % 10;

            int sum = num1 + num2 + carry;
            carry = sum / 10;
            res.add(sum % 10);

            l1--;
            k=k/10;
        }
        if(carry != 0) res.add(carry);
        Collections.reverse(res);
        return res;
    }

    public static void main(String[] args) {
        int[] num = new int[]{2,3,8};
        int k = 89;
        System.out.println(addToArrayForm(num, k));
    }
}
