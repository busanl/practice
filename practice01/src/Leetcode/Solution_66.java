package Leetcode;

public class Solution_66 {
    public static int[] plusOne(int[] digits) {
        int endnum = digits.length - 1;
        while(digits[endnum] == 9){
            digits[endnum] = 0;
            endnum = endnum - 1;
            if(endnum < 0){
                digits = new int[digits.length + 1];
                digits[0] = 1;
                return digits;
            }
        }
        int endnumadd = digits[endnum] + 1;
        digits[endnum] = endnumadd;
        return digits;
    }

    public static void main(String[] args) {
        int[] digits = new int[]{9,9};
        int[] nums = plusOne(digits);
        for (int num : nums){
            System.out.print(num+" ");
        }
    }
}
